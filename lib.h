#ifndef LIB
#define LIB

#define MSGSIZE 250
#define TIMEOUT 5

#define SENDINIT 'S'
#define FILEHEADER 'F'
#define DATA_TRANSMISSION 'D'
#define END_OF_FILE 'Z'
#define END_OF_TRANSMISSION 'B'
#define ACK 'Y'
#define NAK 'N'
#define ERROR 'E'

typedef struct {
    int len;
    char payload[1400];
} msg;

void init(char* remote, int remote_port);
void set_local_port(int port);
void set_remote(char* ip, int port);
int send_message(const msg* m);
int recv_message(msg* r);
msg* receive_message_timeout(int timeout); //timeout in milliseconds
unsigned short crc16_ccitt(const void *buf, int len);

typedef struct {
	char SOH;
	unsigned char LEN;
	unsigned char SEQ;
	char TYPE;
	char DATA[MSGSIZE];
	unsigned short int CHECK;
	char MARK;
} my_pkt;

typedef struct {
	unsigned char MAXL;
	char TIME;
	char NPAD;
	char PADC;
	char EOL;
	char QCTL, QBIN, CHKT, REPT, CAPA, R;

} Data;

#endif

