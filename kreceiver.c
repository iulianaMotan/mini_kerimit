#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include "lib.h"

#define HOST "127.0.0.1"
#define PORT 10001

#define CORRECT 'c'
#define INCORRECT 'i'

void send_response(msg*, char, char*, size_t, unsigned char, char);
char check_message(msg*, msg*, my_pkt*, char, unsigned char, char);
char handle_incorrect_message(msg*, msg*, my_pkt*, char, unsigned char, char, char);
char receive_data(msg*, my_pkt*, char, unsigned char);
void build_message(msg*, my_pkt);
void unpack_message(msg*, my_pkt*);
char receive_init(msg*, msg*, my_pkt*, char, unsigned char, char);
void get_message(my_pkt*, char*);

int main(int argc, char** argv) {
    msg r, last_response;
    my_pkt *p;
    Data *d;

    d = (Data*) malloc (sizeof(Data));
    p = (my_pkt*) malloc(sizeof(my_pkt));

    d->MAXL = MSGSIZE;
    d->TIME = TIMEOUT;
    d->EOL = 12;
    char message, buffer[MSGSIZE], filename[MSGSIZE];
    unsigned char seq_number = 0;
    short int fd;

    init(HOST, PORT);
    printf("[RECEIVER] Starting receiving files.\n");

    /* get initial package */
    message = receive_init(&r, &last_response, p, d->TIME, seq_number, d->EOL);
    if(message == ERROR) {
        return -1;
    }
    printf("[RECEIVER] Received initial data.\n");
    send_response(&last_response, ACK, (char*) d, sizeof(Data), p->SEQ, d->EOL);
     seq_number = p->SEQ + 1;

    while(1) {
        message = check_message(&r, &last_response, p, d->TIME, seq_number, d->EOL);
        if(message == ERROR){
            return -1;
        }
        if(p->TYPE == FILEHEADER) {
            printf("[RECEIVER] Received file header.\n");
            send_response(&last_response, ACK, "", 0, p->SEQ, d->EOL);
            /* get the file name and create the new file */
            get_message(p, buffer);
            sprintf(filename, "recv_%s", buffer);
            seq_number = p->SEQ + 1;
            /* open file for writing */
            fd = open(filename, O_CREAT | O_WRONLY | O_TRUNC, 0644);
            printf("[RECEIVER] Receiving file %s\n", filename);
        }

        else if(p->TYPE == END_OF_FILE) {
            printf("[RECEIVER] Received file %s\n", filename);
            send_response(&last_response, ACK, "", 0, p->SEQ, d->EOL);
            seq_number = p->SEQ + 1;
            close(fd);
        }

        else if(p->TYPE == END_OF_TRANSMISSION) {
            printf("[RECEIVER] Encountered end of transmission.\n");
            send_response(&last_response, ACK, "", 0, p->SEQ, d->EOL);
            break;
        }
        else {
            send_response(&last_response, ACK, "", 0, p->SEQ, d->EOL);
            write(fd, p->DATA, p->LEN - 5);
            seq_number = p->SEQ + 1; 
        }
    }
    
	return 0;
}

void get_message(my_pkt *p, char *msg) {
    memcpy(msg, p->DATA, p->LEN - 5);
    msg[p->LEN - 5] = 0;
}

char receive_init(msg *r, msg *last_response, my_pkt *p, char timeout, unsigned char seq_number, char EOL) {
    unsigned short int count = 1;
    char response;

    response = receive_data(r, p, timeout, seq_number);
    if(response == CORRECT) {
      return CORRECT;
    }
    while(count <= 2) {
        if(response == INCORRECT){
            count = 0;
            send_response(last_response, NAK, "", 0, seq_number, EOL);
            seq_number = (seq_number + 1) % 256;
        }
        response = receive_data(r, p, timeout, seq_number);
        if(response == CORRECT) {
            return CORRECT;
        }
        count++;
    }
    return ERROR;
}

char receive_data(msg *r, my_pkt *p, char timeout, unsigned char seq_number) {
    unsigned short int crc;

    r = receive_message_timeout(timeout * 1000);
    if(r == NULL)
        return ERROR;
    else{
        unpack_message(r, p);
        crc = crc16_ccitt(p, 4 * sizeof(char) + p->LEN - 5);
        if(crc == p->CHECK && (p->SEQ == seq_number)) {
            return CORRECT;
        }
    }
    return INCORRECT;
}

char handle_incorrect_message(msg *r, msg* last_response, my_pkt *p, char timeout, unsigned char seq_number, char message, char EOL) {
    unsigned char response = message, count = 1;
    
    count = 1;
    while(count <= 3) {
        if(response == ERROR) {
            printf("[RECEIVER] Retransmit last package\n");
            send_message(last_response);
        }
        else {
            count = 0;
            send_response(last_response, NAK, "", 0, seq_number, EOL);
            seq_number = (seq_number + 1) % 256;;
        }
        response = receive_data(r, p, timeout, seq_number);
        if(response == CORRECT) {
            return response;
        }
        count++;       
    }

    return ERROR;
}


char check_message(msg *r, msg *last_response, my_pkt *p, char timeout, unsigned char seq_number, char EOL) {
    /* verify the message from the sender */
    char message;
    message = receive_data(r, p, timeout, seq_number);
    if(message == CORRECT){
        return message;
    }
    return handle_incorrect_message(r, last_response, p, timeout, seq_number, message, EOL);
}

void send_response(msg *t, char type, char *response, size_t message_length, unsigned char seq_number, char EOL) {
    my_pkt p;

    memset(t->payload, 0, sizeof(t->payload));
	p.SOH = 1;
    p.SEQ = seq_number;
	p.TYPE = type;
    memcpy(p.DATA, response, message_length);
  	/* to the length of the DATA we will add SEQ, LEN, CHECK and MARK fields */  
    p.LEN = message_length + 3 * sizeof(char) + sizeof(unsigned short int);
    /* crc sum will be computed on the first four bytes and on the DATA field */
    p.CHECK = crc16_ccitt(&p, 4 * sizeof(char) + p.LEN - 5);
    p.MARK = EOL;
	build_message(t, p);
  	send_message(t);
    if(type == ACK){
        printf("[RECEIVER] ACK %d\n", seq_number);
    }
    else{
        printf("[RECEIVER] NAK %d\n", seq_number);
    }
}

void build_message(msg *t, my_pkt p) {
    unsigned short int length = 4 * sizeof(char);
    memset(t->payload, 0, sizeof(t->payload));
    /* add the first four bytes to the payload */
    memcpy(t->payload, &p, 4 * sizeof(char));
    /* copy the data field */
    memcpy(t->payload + length, p.DATA, p.LEN - 5);
    length += p.LEN - 5;
    memcpy(t->payload + length, &p.CHECK, sizeof(unsigned short int));
    length += sizeof(unsigned short int);
    memcpy(t->payload + length, &p.MARK, sizeof(char));
    length += sizeof(char);
    t->len = length;
}

void unpack_message(msg *t, my_pkt *p) {
    unsigned short int length = 4 * sizeof(char);
    /* copy the first four characters */
    memcpy(p, t->payload, 4 * sizeof(char));
    /* copy the data field */
    memcpy(p->DATA, t->payload + length, p->LEN - 5);
    length += p->LEN - 5;
    memcpy(&p->CHECK, t->payload + length, sizeof(unsigned short int));
    length += sizeof(unsigned short int);
    memcpy(&p->MARK, t->payload + length, sizeof(char));
}
