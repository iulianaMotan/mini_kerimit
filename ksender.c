#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "lib.h"

#define HOST "127.0.0.1"
#define PORT 10000

char check_response(msg*, char, unsigned char);
char retransmit_message(msg*, char, unsigned char, char);
char get_response(char, unsigned char);
void send_init(msg*, Data*);
void send_data(msg*, char, unsigned char, char, char*, size_t);
void build_message(msg*, my_pkt);

void unpack_message(msg *t, my_pkt *p);


int main(int argc, char** argv) {
    msg t;
    Data d;
    size_t count = 0;
    short int fd, i = 1;
    char buffer[MSGSIZE];
    unsigned char seq_number = 0;

    d.MAXL = MSGSIZE;
    d.TIME = TIMEOUT;
    d.NPAD = 0;
    d.PADC = 0;
    d.EOL = 13;
    d.QCTL = d.QBIN = d.CHKT = d.REPT = d.CAPA = d.R = 0;

    init(HOST, PORT);
    
    /* send send-init message */
    send_init(&t, &d);
    printf("[SENDER] Send-init package sent.\n");
    
    /* verify the response from the receiver */
    seq_number = check_response(&t, d.TIME, seq_number);
    if(seq_number == 0){
       return -1;
    }
    
    while(i < argc){
        printf("[SENDER] Sending file %s\n", argv[i]);
        fd = open(argv[i], O_RDONLY);

        send_data(&t, d.EOL, seq_number, FILEHEADER, argv[i], strlen(argv[i])); 
        printf("[SENDER] File header sent.\n");
        /* verify the response from the receiver */
        seq_number = check_response(&t, d.TIME, seq_number);
        if(seq_number == 0)
            return -1;

        while ((count = read(fd, buffer, d.MAXL)) > 0) {
            send_data(&t, d.EOL, seq_number, DATA_TRANSMISSION, buffer, count); 
            /* verify the response from the receiver */
            seq_number = check_response(&t, d.TIME, seq_number);
            if(seq_number == 0){
                return -1;
            }
        }

        send_data(&t, d.EOL, seq_number, END_OF_FILE, "", 0); 
        printf("[SENDER] End of file sent.\n");
        /* verify the response from the receiver */
        seq_number = check_response(&t, d.TIME, seq_number);
        if(seq_number == 0){
            return -1;
        }
        i++;
    }

    send_data(&t, d.EOL, seq_number, END_OF_TRANSMISSION, "", 0); 
    printf("[SENDER] End of transmission package sent.\n");
    /* verify the response from the receiver */
    seq_number = check_response(&t, d.TIME, seq_number);
    if(seq_number == -1){
        return -1;
    }

    return 0;
}

void send_data(msg *t, char EOL, unsigned char seq_number, char type, char *data, size_t data_length) {
    my_pkt p;
    p.SOH = 1;
    p.SEQ = seq_number;
    p.TYPE = type;  
    memcpy(p.DATA, data, data_length);
     /* to the length of the DATA we will add SEQ, LEN, CHECK and MARK fields */  
    p.LEN = data_length + 3 * sizeof(char) + sizeof(unsigned short int);
    /* crc sum will be computed on the first four bytes and on the DATA field */ 
    p.CHECK = crc16_ccitt(&p, 4 * sizeof(char) + p.LEN - 5);
    p.MARK = EOL;
    /* build the message to send */
    build_message(t, p);
    /* send the send-init message */
    send_message(t);
    printf("[SENDER] Package %d\n", seq_number);
}

void send_init(msg *t, Data *d) {
    my_pkt p;
    /* build my_pkt variable */
    p.SOH = 1;
    p.SEQ = 0;
    p.TYPE = SENDINIT;
    memcpy(p.DATA, d, sizeof(Data));
    /* to the length of the DATA we will add SEQ, LEN, CHECK and MARK fields */  
    p.LEN = sizeof(Data) + 3 * sizeof(char) + sizeof(unsigned short int);
    /* crc sum will be computed on the first four bytes and on the DATA field */
    p.CHECK = crc16_ccitt(&p, 4 * sizeof(char) + sizeof(Data));
    p.MARK = d->EOL;
    /* build the message to send */
    build_message(t, p);
    /* send the send-init message */
    send_message(t);
    printf("[SENDER] Package %d\n", 0);
}

char get_response(char timeout, unsigned char seq_number) {
    msg *r;
    r = (msg*) malloc(sizeof(msg));
    my_pkt p;
    /* wait for response */
    r = receive_message_timeout(timeout * 1000 + 5);
    if (r == NULL) {
        return ERROR;
    } 
    /* the response was received in time */
    unpack_message(r, &p);
    if(p.SEQ == seq_number) {
        return p.TYPE;
    }
    return ERROR;
}
char retransmit_message(msg *t, char timeout, unsigned char current_seq, char response) {
    unsigned char count = 1;
    my_pkt p;

    while(count <= 3) {
        if(response == NAK) {
            count = 0;
            unpack_message(t, &p);
            current_seq++;
            p.SEQ = current_seq;
            p.CHECK = crc16_ccitt(&p, 4 * sizeof(char) + p.LEN - 5);
            build_message(t, p);
        }
        printf("[SENDER] Retransmit package %d\n", current_seq);
        send_message(t);
        response = get_response(timeout, current_seq);
        /* if we got ACK, the message had been sent correctly */
        if(response == ACK) {
            return (current_seq + 1);
        }
        count++;
    }

    return 0;
}

char check_response(msg *t, char timeout, unsigned char seq_number) {
    /* verify the response from the receiver */
    char response;
    response = get_response(timeout, seq_number);
    if(response == ACK){
        return (seq_number + 1);
    }
    return retransmit_message(t, timeout, seq_number, response);
}

void build_message(msg *t, my_pkt p) {
    unsigned short int length = 4 * sizeof(char);
    memset(t->payload, 0, sizeof(t->payload));
    /* add the first four bytes to the payload */
    memcpy(t->payload, &p, 4 * sizeof(char));
    /* copy the data field */
    memcpy(t->payload + length, p.DATA, p.LEN - 5);
    length += p.LEN - 5;
    memcpy(t->payload + length, &p.CHECK, sizeof(unsigned short int));
    length += sizeof(unsigned short int);
    memcpy(t->payload + length, &p.MARK, sizeof(char));
    length += sizeof(char);
    t->len = length;
}

void unpack_message(msg *t, my_pkt *p) {
    unsigned short int length = 4 * sizeof(char);
    /* copy the first four characters */
    memcpy(p, t->payload, 4 * sizeof(char));
    /* copy the data field */
    memcpy(p->DATA, t->payload + length, p->LEN - 5);
    length += p->LEN - 5;
    memcpy(&p->CHECK, t->payload + length, sizeof(unsigned short int));
    length += sizeof(unsigned short int);
    memcpy(&p->MARK, t->payload + length, sizeof(char));
}